﻿<?php //header('Content-type: application/json; charset=utf-8'); 
 //error_reporting(0);
 
 //ini_set('allow_url_fopen', '1');
 
 $SMS="true";
 ini_set('default_charset', "utf-8");
require_once 'config.php';
$con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);

mysql_set_charset('utf8', $con);

mysql_select_db(DB_DATABASE);
mysql_query("SET NAMES utf8;
			 SET CHARACTER SET utf8;
			");

//global $specializations;
$specializations=Array('Error');
$query_spec="select * from specializations";
$spec_result=mysql_query($query_spec) or die(mysql_error());
while($spec=mysql_fetch_array($spec_result))
	$specializations[]=$spec[1];
//print_r($specializations);

//global $qualifications;
$qualifications=array('Error');
$query_qualif="select * from qualifications";
$qualif_result=mysql_query($query_qualif) or die(mysql_error());
while($quali=mysql_fetch_array($qualif_result))
	$qualifications[]=$quali[1];

// print_r($qualifications);
 



function send_sms($text, $phone){
	if(strlen($pin)==4){
		if(strlen($phone)==13){
			$xml="<?xml version=\"1.0\" encoding=\"utf-8\" ?>
				<package key=\"f8d5770c4fee0d8af04fc9f39c51c42856e9cd7b\">
					<message>
						<msg recipient= \"$phone\" sender= \"Barberland\" type=\"0\">$text</msg>
					</message>
				</package>";
 
		//echo htmlspecialchars($xml);
		$url = "http://alphasms.com.ua/api/xml.php";

        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
		// Following line is compulsary to add as it is:
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "" . $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);
		//convert the XML result into array
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
        // print_r('<pre>');
        // print_r($array_data);
		//print_r('</pre>');			
		return $array_data;
		}
	}
}


function getCoordinates($address){
	$address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
	$url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
	$response = file_get_contents($url);
	$json = json_decode($response,TRUE); //generate array object from the response from the web
	return ( array($json['results'][0]['geometry']['location']['lat'],$json['results'][0]['geometry']['location']['lng']));
}

function makePassword( $type){
	// Characters to use for the password
	switch($type){
		case "pin":
			$pwlen = 4;
			$str="0123456789";
			break;
		case "password":
		default:
			$pwlen = 12;
			$str = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789";
			break;
	}
	// Length of the string to take characters from
	$len = strlen($str);
	 // RANDOM.ORG - We are pulling our list of random numbers as a
	// single request, instead of iterating over each character individually
	$uri = "http://www.random.org/integers/?";
	$random = file_get_contents(
    	$uri ."num=$pwlen&min=0&max=".($len-1)."&col=1&base=10&format=plain&rnd=new"
	);
	$indexes = explode("\n", $random);
	array_pop(&$indexes);
 	// We now have an array of random indexes which we will use to build our password
	$pw = '';
	foreach ($indexes as $int){
    	$pw .= substr($str, $int, 1);
	}
	// Password is stored in `$pw`
	 return $pw;
 }

 
$index = 0;
function showBarbers($query){
	$specializations=$GLOBALS['specializations'];
	$qualifications=$GLOBALS['qualifications'];
	$result=mysql_query($query) or die(mysql_error());
	$barbers=array();
		
		while($row=mysql_fetch_array($result, MYSQL_ASSOC)):
			$shedule=mysql_fetch_array(mysql_query("select * from barber_shedules where barber_id=".$row['id']),MYSQL_ASSOC);
			$row['shedule'] = $shedule;
			$prices=mysql_query("select * from barber_prices where barber_id=".$row['id'])or die("Unable to get barbers' prices".mysql_error());
			$price_array=mysql_fetch_array($prices, MYSQL_ASSOC);
			unset($price_array['id']);
			unset($price_array['barber_id']);
			$row['prices']=$price_array;
			$specialization=mysql_query("select specialization_id from barbers_specialization where barber_id=".$row['id']);
			$barbers_specializations=Array();
			while($sps=mysql_fetch_array($specialization)){
				$barbers_specializations[]=$specializations[$sps[specialization_id]];
			}
			$row['specializations']=$barbers_specializations;
			
			$qualification=mysql_query("select qualification_id from barbers_qualification where barber_id=".$row['id']);
			$qfl=mysql_fetch_array($qualification);
			$row['qualification']=$qualifications[$qfl[0]];
			
			$barbers[++$GLOBALS['index']]=$row;
		endwhile;
		mysql_free_result($result);
		return $barbers;	
}
$sindex=0;
function showSalons($query){
	$specializations=$GLOBALS['specializations'];
	$qualifications=$GLOBALS['qualifications'];
	$result=mysql_query($query) or die("bad showSalons ".mysql_error());
	$barbers=array();
		
		while($row=mysql_fetch_array($result, MYSQL_ASSOC)):
			$tmp=array();
			$tmp['home_lat']=$row['lat'];
			$tmp['home_long']=$row['lng'];
			$tmp['name']=$row['name'];
			$tmp['salon_address']=$row['address'];
			$tmp['phone_number']=$row['phone'];
			$tmp['photo_path']=$row['logo_path'];
			$tmp['shedule_text']=$row['shedule_text'];
			$tmp['comments']=$row['comments'];
			$tmp['second_name']="";
			$tmp['id']=$row['id'];
			$tmp['email']=$row['email'];
			$tmp['isSalon']='true';
			
			$shedule=mysql_fetch_array(mysql_query("select * from salon_shedules where salon_id=".$row['id']),MYSQL_ASSOC);
			$tmp['shedule'] = $shedule;
			
			$prices=mysql_query("select * from salon_prices where salon_id=".$row['id'])or die("Unable to get salons' prices".mysql_error());
			$price_array=mysql_fetch_array($prices, MYSQL_ASSOC);
			unset($price_array['id']);
			unset($price_array['salon_id']);
			$tmp['prices']=$price_array;
			$specialization=mysql_query("select specialization_id from salon_specializations where salon_id=".$row['id']);
			$barbers_specializations=Array();
			while($sps=mysql_fetch_array($specialization)){
				$barbers_specializations[]=$specializations[$sps[specialization_id]];
			}
			$tmp['specializations']=$barbers_specializations;
			
			$tmp['qualification']=$qualifications[4];
			$barbers[++$GLOBALS['sindex']]=$tmp;
		endwhile;
		mysql_free_result($result);
		//print_r($row);
		return $barbers;	
}

 
if (file_get_contents('php://input')) {
    // Get the JSON Array
    $json = file_get_contents('php://input');
    // Lets parse through the JSON Array and get our individual values
    // in the form of an array
    $parsedJSON = json_decode($json, true);
    $dataType = (isset($parsedJSON['dataType'])) ? $parsedJSON['dataType'] : '';
	
	if(strlen($dataType)>0&&$dataType=="searchBarberName"){
		$barberNameToSearch = (isset($parsedJSON['barberNameToSearch'])) ? $parsedJSON['barberNameToSearch'] : '';
		$barberz=array_merge(showBarbers("select * from barbers where name like '$barberNameToSearch' or second_name like '$barberNameToSearch'"),
							 showSalons( "select * from salons  where name like '$barberNameToSearch' or comments    like '$barberNameToSearch'"));
		for($i=1;$i<=count($barberz);$i++)$indexez[]="$i";
		echo json_encode(array_combine($indexez,$barberz));
	}
	
	
	
	//callback
	if(strlen($dataType)>0&&$dataType=="searchBarberName"){
		if($SMS==true){
			$barber_phone = (isset($parsedJSON['barber_phone'])) ? $parsedJSON['barber_phone'] : '';
			$client_phone = (isset($parsedJSON['client_phone'])) ? $parsedJSON['client_phone'] : '';
			//echo send_sms("Barberland\nперезвоните на\n"+$client_phone,$barber_phone);
		}
	}	
	
	//showMeTheBarber
	if(strlen($dataType)>0&&$dataType=="showMeTheBarber"){
		$phone = (isset($parsedJSON['phone']))   ? $parsedJSON['phone'] :   '0';
		$barberz=array_merge(showBarbers("select * from barbers where phone_number like '$phone'"),
							 showSalons( "select * from salons  where phone like '$phone'"));
		for($i=1;$i<=count($barberz);$i++)$indexez[]="$i";
		echo json_encode(array_combine($indexez,$barberz));
	}
	
	
	//searchByPrice
	if(strlen($dataType)>0&&$dataType=="searchByPrice"){
		$mc_from = (isset($parsedJSON['search_prices_man_cut_from']))   ? $parsedJSON['search_prices_man_cut_from'] :   '0';
    	$wc_from = (isset($parsedJSON['search_prices_woman_cut_from'])) ? $parsedJSON['search_prices_woman_cut_from'] : '0';
    	$co_from = (isset($parsedJSON['search_prices_colorize_from']))  ? $parsedJSON['search_prices_colorize_from'] :  '0';
    	$ha_from = (isset($parsedJSON['search_prices_hairdo_from']))    ? $parsedJSON['search_prices_hairdo_from'] :    '0';
		$mc_to   = (isset($parsedJSON['search_prices_man_cut_to']))     ? $parsedJSON['search_prices_man_cut_to'] :   '999999';
    	$wc_to   = (isset($parsedJSON['search_prices_woman_cut_to']))   ? $parsedJSON['search_prices_woman_cut_to'] : '999999';
    	$co_to   = (isset($parsedJSON['search_prices_colorize_to']))    ? $parsedJSON['search_prices_colorize_to'] :  '999999';
    	$ha_to   = (isset($parsedJSON['search_prices_hairdo_to']))      ? $parsedJSON['search_prices_hairdo_to'] :    '999999';
		$price_query="SELECT barber_id FROM `barber_prices` WHERE (man_cut between $mc_from and $mc_to) and ";		
		$price_query.=                                            "(woman_cut between $wc_from and $wc_to) and ";
		$price_query.=                                            "(colorize between $co_from and $co_to) and ";		
		$price_query.=                                            "(hairdo between $ha_from and $ha_to)";
		$price_result=mysql_query($price_query) or die("Bad search_price_barber query ".mysql_error());
		$barbers=array();
		$k=0;
		$indexes=Array();
		while($row=mysql_fetch_row($price_result)):
			$indexes[]="".++$k;		
			$barbers=array_merge(showBarbers("select * from barbers where id=".$row[0]),$barbers);
		endwhile; 
		//echo count($indexes)."|".count($barbers)."|".$k."<BR>";
		$salon_query="SELECT salon_id FROM `salon_prices` WHERE (man_cut between $mc_from and $mc_to) and ";		
		$salon_query.=                                            "(woman_cut between $wc_from and $wc_to) and ";
		$price_query.=                                            "(colorize between $co_from and $co_to) and ";		
		$salon_query.=                                            "(hairdo between $ha_from and $ha_to)";
		$salon_result=mysql_query($salon_query) or die("Bad search_price_salon query ".mysql_error());
	
		while($row=mysql_fetch_row($salon_result)):
			$indexes[]="".++$k;
			$barbers=array_merge(showSalons("select * from salons where id=".$row[0]),$barbers);
		endwhile; 
		//echo count($indexes)."|".count($barbers)."|".$k;
		echo json_encode(array_combine($indexes,$barbers));
		
	}	
	
	if(strlen($dataType)>0&&$dataType=="searchBarberQualification"){
	
		$qualificationToSearch = (isset($parsedJSON['qualificationToSearch'])) ? $parsedJSON['qualificationToSearch'] : '';
		//$qual_id= array_search($qualificationToSearch,$qualifications);
		$query="select barber_id from barbers_qualification where qualification_id=$qualificationToSearch";
		
		$result=mysql_query($query) or die(mysql_error());
		$barbers=array();
		$k=0;
		$indexes=Array();
		while($row=mysql_fetch_row($result)):
			$indexes[]="".++$k;
			$barbers=array_merge(showBarbers("select * from barbers where id=".$row[0]),$barbers);
		endwhile; 
		//echo "zzzzz ".count($indexes)."|".count($barbers);
		echo json_encode(array_combine($indexes,$barbers));
	}
	
	if(strlen($dataType)>0&&$dataType=="searchBarberSpecialization"){
	
		$specializationToSearch = (isset($parsedJSON['specializationToSearch'])) ? $parsedJSON['specializationToSearch'] : '';
		$arr=explode("|",$specializationToSearch);
		$str="";
		for($z=0;$z<count($arr);$z++){
			if(strlen($str)>0)$str.=", ";
			$str.=$arr[$z];
		}
		
		$barberz=Array();
		$indexez=Array();
		
		$query="select distinct barber_id from barbers_specialization where specialization_id in ($str) ORDER BY barber_id";
		//print_r($arr);
		// "zzzzz ".$specializationToSearch."(".count($arr).")zzzzzz".$str." zzzzz";
		$result=mysql_query($query) or die(mysql_error());
		while($row=mysql_fetch_row($result)):
			$barberz=array_merge(showBarbers("select * from barbers where id=".$row[0]),$barberz);
		endwhile; 
		
		$query="select distinct salon_id from salon_specializations where specialization_id in ($str) ORDER BY salon_id";
		$result=mysql_query($query) or die(mysql_error());
		while($row=mysql_fetch_row($result)):
			$barberz=array_merge(showSalons("select * from salons where id=".$row[0]),$barberz);
		endwhile; 
		
		for($i=1;$i<=count($barberz);$i++)$indexez[]="$i";
		
		echo json_encode(array_combine($indexez,$barberz));
	}
	
	if(strlen($dataType)>0&&$dataType=="specAndQualif"){	
			$data=Array();
			$data['specializations'] = $specializations;
			$data['qualifications']  = $qualifications;
			echo json_encode($data);
	}
	
     if(     strlen($dataType)>0&&$dataType=="newClient"){
     	     $clientName = (isset($parsedJSON['clientName'])) ? $parsedJSON['clientName'] : '';
    		 $clientPhone = (isset($parsedJSON['clientPhone'])) ? $parsedJSON['clientPhone'] : '';
			 $result=mysql_query("select * from clients where phone=$clientPhone");
			 $newPin=makePassword("pin");
			 if( mysql_num_rows($result)==0){
				
				$res=mysql_query("INSERT INTO clients(name,
								phone,
								pin,
								comments,
								registration_date)values('$clientName',
														'$clientPhone',
														'$newPin',
														'',
														NOW())")or die('Bad client insert: ' . mysql_error());
				if($SMS=="true")
                                     echo "pin:".$newPin.":".send_sms("Barberland pin\n".$newPin, $clientPhone);
                                else
                                     echo "pin:".$newPin;
			 }else {
				 $res=mysql_query("UPDATE `clients` SET `name`='$clientName', 
										  `pin`='$newPin',
										  `registration_date`=NOW() 
										  WHERE phone='$clientPhone';")
										  or die('Bad client update: ' . mysql_error());
				if($SMS=="true")
                                     echo "pin:".$newPin.":".send_sms("Barberland pin\n".$newPin, $clientPhone);
                                else
                                     echo "pin:".$newPin;
				
			 }
	}
   if(strlen($dataType)>0&&$dataType=="newBarber"){
    	
    	$barberName = (isset($parsedJSON['barberName'])) ? $parsedJSON['barberName'] : '';
    	$barberPhone = (isset($parsedJSON['barberPhone'])) ? $parsedJSON['barberPhone'] : '';
    	$barberSecondName = (isset($parsedJSON['barberSecondName'])) ? $parsedJSON['barberSecondName'] : '';
    	$barberEmail = (isset($parsedJSON['barberEmail'])) ? $parsedJSON['barberEmail'] : '';
		$uploadedFilePath = (isset($parsedJSON['uploadedFilePath'])) ? $parsedJSON['uploadedFilePath'] : '';
    	$salonName = (isset($parsedJSON['salonName'])) ? $parsedJSON['salonName'] : '';
    	$salonAddr = (isset($parsedJSON['salonAddr'])) ? $parsedJSON['salonAddr'] : '';
    	$salonPhone = (isset($parsedJSON['salonPhone'])) ? $parsedJSON['salonPhone'] : '';
		$isViber = (isset($parsedJSON['isViber'])) ? $parsedJSON['isViber'] : '';
    	$isMonday = (isset($parsedJSON['isMonday'])) ? $parsedJSON['isMonday'] : '';
    	$isWednesday = (isset($parsedJSON['isWednesday'])) ? $parsedJSON['isWednesday'] : '';
    	$isTuesday = (isset($parsedJSON['isTuesday'])) ? $parsedJSON['isTuesday'] : '';
		$isThursday = (isset($parsedJSON['isThursday'])) ? $parsedJSON['isThursday'] : '';
    	$isFriday = (isset($parsedJSON['isFriday'])) ? $parsedJSON['isFriday'] : '';
    	$isSaturday = (isset($parsedJSON['isSaturday'])) ? $parsedJSON['isSaturday'] : '';
    	$isSunday = (isset($parsedJSON['isSunday'])) ? $parsedJSON['isSunday'] : '';
		$isOdd = (isset($parsedJSON['isOdd'])) ? $parsedJSON['isOdd'] : '';
    	$isEven = (isset($parsedJSON['isEven'])) ? $parsedJSON['isEven'] : '';
 		$monOpenTime = (isset($parsedJSON['monOpenTime'])) ? $parsedJSON['monOpenTime'] : '';
    	$monFinishTime = (isset($parsedJSON['monFinishTime'])) ? $parsedJSON['monFinishTime'] : '';
		$tueOpenTime = (isset($parsedJSON['tueOpenTime'])) ? $parsedJSON['tueOpenTime'] : '';
    	$tueFinishTime = (isset($parsedJSON['tueFinishTime'])) ? $parsedJSON['tueFinishTime'] : '';
    	$wedOpenTime = (isset($parsedJSON['wedOpenTime'])) ? $parsedJSON['wedOpenTime'] : '';
    	$wedFinishTime = (isset($parsedJSON['wedFinishTime'])) ? $parsedJSON['wedFinishTime'] : '';
		$thuOpenTime = (isset($parsedJSON['thuOpenTime'])) ? $parsedJSON['thuOpenTime'] : '';
   		$thuFinishTime = (isset($parsedJSON['thuFinishTime'])) ? $parsedJSON['thuFinishTime'] : '';
    	$friOpenTime = (isset($parsedJSON['friOpenTime'])) ? $parsedJSON['friOpenTime'] : '';
    	$friFinishTime = (isset($parsedJSON['friFinishTime'])) ? $parsedJSON['friFinishTime'] : '';
		$satOpenTime = (isset($parsedJSON['satOpenTime'])) ? $parsedJSON['satOpenTime'] : '';
    	$satFinishTime = (isset($parsedJSON['satFinishTime'])) ? $parsedJSON['satFinishTime'] : '';
    	$sunOpenTime = (isset($parsedJSON['sunOpenTime'])) ? $parsedJSON['sunOpenTime'] : '';
    	$sunFinishTime = (isset($parsedJSON['sunFinishTime'])) ? $parsedJSON['sunFinishTime'] : '';
		$oddOpenTime = (isset($parsedJSON['oddOpenTime'])) ? $parsedJSON['oddOpenTime'] : '';
    	$oddFinishTime = (isset($parsedJSON['oddFinishTime'])) ? $parsedJSON['oddFinishTime'] : '';
    	$evenOpenTime = (isset($parsedJSON['evenOpenTime'])) ? $parsedJSON['evenOpenTime'] : '';
    	$evenFinishTime = (isset($parsedJSON['evenFinishTime'])) ? $parsedJSON['evenFinishTime'] : '';
		$qualification = (isset($parsedJSON['qualification'])) ? $parsedJSON['qualification'] : '';

    	$isManCut = (isset($parsedJSON['isManCut'])) ? $parsedJSON['isManCut'] : '';
    	$isWomanCut = (isset($parsedJSON['isWomanCut'])) ? $parsedJSON['isWomanCut'] : '';
		$isEvening = (isset($parsedJSON['isEvening'])) ? $parsedJSON['isEvening'] : '';
    	$isColor = (isset($parsedJSON['isColor'])) ? $parsedJSON['isColor'] : '';
    	$isHighlight = (isset($parsedJSON['isHighlight'])) ? $parsedJSON['isHighlight'] : '';
		
		$manCutPrice = (isset($parsedJSON['manCutPrice'])) ? $parsedJSON['manCutPrice'] : '';
    	$womanCutPrice = (isset($parsedJSON['womanCutPrice'])) ? $parsedJSON['womanCutPrice'] : '';
		$colorizePrice = (isset($parsedJSON['colorizePrice'])) ? $parsedJSON['colorizePrice'] : '';
    	$hairdoPrice = (isset($parsedJSON['hairdoPrice'])) ? $parsedJSON['hairdoPrice'] : '';
		

	
		$shedule="";
		$shedule_names="`barber_id`";
		$shedule_values="";
    if($isOdd){
        $shedule.="По четным с $evenOpenTime по $evenFinishTime\n";
        $shedule_names.=", `isOdd`, `oddOpenTime`, `oddFinishTime`";
        $shedule_values.=", $isOdd, $oddOpenTime, $oddFinishTime";
    }
    if($isEven){
        $shedule.="По четным с $evenOpenTime до $evenFinishTime\n";
        $shedule_names.=", `isEven`, `evenOpenTime`, `evenFinishTime`";
        $shedule_values.=", $isEven, $evenOpenTime, $evenFinishTime";
    }
    if($isMonday){
        $shedule.=   "Понедельник с $monOpenTime до $monFinishTime\n";
        $shedule_names.=", `isMonday`, `monOpenTime`, `monFinishTime`";
        $shedule_values.=", $isMonday, $monOpenTime,  $monFinishTime";
    }
    if($isTuesday){
        $shedule.=  "Вторник     с $tueOpenTime до $tueFinishTime\n";
        $shedule_names.=", `isTuesday`, `tueOpenTime`, `tueFinishTime`";
        $shedule_values.=", $isTuesday, $tueOpenTime,  $tueFinishTime";
    }
    if($isWednesday){
        $shedule.="Среда       с $wedOpenTime до $wedFinishTime\n";
        $shedule_names.=", `isWednesday`, `wedOpenTime`, `wedFinishTime`";
        $shedule_values.=", $isWednesday, $wedOpenTime,  $wedFinishTime";
    }
    if($isThursday){
        $shedule.= "Четверг     с $thuOpenTime до $thuFinishTime\n";
        $shedule_names.=", `isThursday`, `thuOpenTime`, `thuFinishTime`";
        $shedule_values.=", $isThursday, $thuOpenTime,  $thuFinishTime";
    }
    if($isFriday){
        $shedule.=   "Пятница     с $friOpenTime до $friFinishTime\n";
        $shedule_names.=", `isFriday`, `friOpenTime`, `friFinishTime`";
        $shedule_values.=", $isFriday, $friOpenTime,  $friFinishTime";
    }
    if($isSaturday){
        $shedule.= "Суббота     с $satOpenTime до $satFinishTime\n";
        $shedule_names.=", `isSaturday`, `satOpenTime`, `satFinishTime`";
        $shedule_values.=", $isSaturday, $satOpenTime,  $satFinishTime";
    }
    if($isSunday){
        $shedule.=   "Воскресенье с $sunOpenTime до $sunFinishTime\n";
        $shedule_names.=", `isSunday`, `sunOpenTime`, `sunFinishTime`";
        $shedule_values.=", $isSunday, $sunOpenTime,  $sunFinishTime";
	}

    
    
    
	$newPin=makePassword("pin");
         $coord=getCoordinates($salonAddr);
        $result = mysql_query("INSERT INTO barbers(phone_number,
												   name,
												   second_name,
												   salon_name,
												   salon_address,
												   salon_phone,
												   home_lat,
												   home_long,
												   shedule_text,
												   photo_path,
												   diploma_photo_path,
												   registration_date,
												   password_hash,
												   comments,
												   email) VALUES('$barberPhone',
																 '$barberName',
																 '$barberSecondName',
																 '$salonName',
																 '$salonAddr',
																 '$salonPhone',
																 '$coord[0]',
																 '$coord[1]',
																 '$shedule',
																 '$uploadedFilePath',
																 '',
																 NOW(),
																 '$newPin',
																 '',
																 '$barberEmail')")or die('Bad barber insert: ' . mysql_error());
		$qualif_id=1;
		

		if(stripos($qualification, "master")){
				$qualif_id=3;
		}else  if(stripos($qualification, "stylist")){
						$qualif_id=2;
					}else if(stripos($qualification,  "barber")){
									$qualif_id=1;
								}


		$barber_id=mysql_insert_id();

       $result_qualif=  mysql_query("INSERT INTO `barbers_qualification`(`barber_id`, `qualification_id`) VALUES ($barber_id, $qualif_id)") or die('Bad qualification insert: ' . mysql_error());
        if($isManCut=="true")
       		$result_qualif=  mysql_query("INSERT INTO `barbers_specialization`(`barber_id`, `specialization_id`) VALUES ($barber_id, 1)") or die('Bad specialization 1 insert: ' . mysql_error());
       	 if($isWomanCut=="true")
       		$result_qualif=  mysql_query("INSERT INTO `barbers_specialization`(`barber_id`, `specialization_id`) VALUES ($barber_id, 2)") or die('Bad specialization 2 insert: ' . mysql_error());
       	 if($isEvening=="true")
       		$result_qualif=  mysql_query("INSERT INTO `barbers_specialization`(`barber_id`, `specialization_id`) VALUES ($barber_id, 3)") or die('Bad specialization 3 insert: ' . mysql_error());
       	if($isColor=="true")
       		$result_qualif=  mysql_query("INSERT INTO `barbers_specialization`(`barber_id`, `specialization_id`) VALUES ($barber_id, 4)") or die('Bad specialization 4 insert: ' . mysql_error());
       	if($isHighlight=="true")
       		$result_qualif=  mysql_query("INSERT INTO `barbers_specialization`(`barber_id`, `specialization_id`) VALUES ($barber_id, 5)") or die('Bad specialization 5 insert: ' . mysql_error());
		mysql_query("INSERT INTO `barber_prices`(`barber_id`, `man_cut`, `woman_cut`, `colorize`, `hairdo`) 
		             VALUES ($barber_id, '$manCutPrice','$womanCutPrice','$colorizePrice','$hairdoPrice')")or die ("bad barber price ".mysql_error());
		
		mysql_query("INSERT INTO `barber_shedules`($shedule_names) VALUES (".$barber_id.$shedule_values.")") or die("bad barber_shedule insert ".mysql_error());
		
		echo "pin:".$newPin;
    }
	
	
	if(strlen($dataType)>0&&$dataType=="newSalon"){
    	
    	$newSalonName     = (isset($parsedJSON['newSalonName']))          ? $parsedJSON['newSalonName'] : '';
    	$newSalonPhone    = (isset($parsedJSON['newSalonPhone']))         ? $parsedJSON['newSalonPhone'] : '';
    	$newSalonEmail    = (isset($parsedJSON['newSalonEmail']))         ? $parsedJSON['newSalonEmail'] : '';
		$newSalonAddress  = (isset($parsedJSON['newSalonAddress']))       ? $parsedJSON['newSalonAddress'] : '';
		$uploadedFilePath = (isset($parsedJSON['uploadedSalonFilePath'])) ? $parsedJSON['uploadedSalonFilePath'] : '';
		$comments     = (isset($parsedJSON['comments']))          ? $parsedJSON['comments'] : '';
    	
    	$isMonday    = (isset($parsedJSON['isMonday'])) ? $parsedJSON['isMonday'] : '';
    	$isWednesday = (isset($parsedJSON['isWednesday'])) ? $parsedJSON['isWednesday'] : '';
    	$isTuesday   = (isset($parsedJSON['isTuesday'])) ? $parsedJSON['isTuesday'] : '';
	$isThursday  = (isset($parsedJSON['isThursday'])) ? $parsedJSON['isThursday'] : '';
    	$isFriday    = (isset($parsedJSON['isFriday'])) ? $parsedJSON['isFriday'] : '';
    	$isSaturday  = (isset($parsedJSON['isSaturday'])) ? $parsedJSON['isSaturday'] : '';
    	$isSunday    = (isset($parsedJSON['isSunday'])) ? $parsedJSON['isSunday'] : '';
	$isOdd       = (isset($parsedJSON['isOdd'])) ? $parsedJSON['isOdd'] : '';
    	$isEven      = (isset($parsedJSON['isEven'])) ? $parsedJSON['isEven'] : '';
 	$monOpenTime = (isset($parsedJSON['monOpenTime'])) ? $parsedJSON['monOpenTime'] : '';
    	$monFinishTime = (isset($parsedJSON['monFinishTime'])) ? $parsedJSON['monFinishTime'] : '';
	$tueOpenTime = (isset($parsedJSON['tueOpenTime'])) ? $parsedJSON['tueOpenTime'] : '';
    	$tueFinishTime = (isset($parsedJSON['tueFinishTime'])) ? $parsedJSON['tueFinishTime'] : '';
    	$wedOpenTime = (isset($parsedJSON['wedOpenTime'])) ? $parsedJSON['wedOpenTime'] : '';
    	$wedFinishTime = (isset($parsedJSON['wedFinishTime'])) ? $parsedJSON['wedFinishTime'] : '';
		$thuOpenTime = (isset($parsedJSON['thuOpenTime'])) ? $parsedJSON['thuOpenTime'] : '';
   		$thuFinishTime = (isset($parsedJSON['thuFinishTime'])) ? $parsedJSON['thuFinishTime'] : '';
    	$friOpenTime = (isset($parsedJSON['friOpenTime'])) ? $parsedJSON['friOpenTime'] : '';
    	$friFinishTime = (isset($parsedJSON['friFinishTime'])) ? $parsedJSON['friFinishTime'] : '';
		$satOpenTime = (isset($parsedJSON['satOpenTime'])) ? $parsedJSON['satOpenTime'] : '';
    	$satFinishTime = (isset($parsedJSON['satFinishTime'])) ? $parsedJSON['satFinishTime'] : '';
    	$sunOpenTime = (isset($parsedJSON['sunOpenTime'])) ? $parsedJSON['sunOpenTime'] : '';
    	$sunFinishTime = (isset($parsedJSON['sunFinishTime'])) ? $parsedJSON['sunFinishTime'] : '';
		$oddOpenTime = (isset($parsedJSON['oddOpenTime'])) ? $parsedJSON['oddOpenTime'] : '';
    	$oddFinishTime = (isset($parsedJSON['oddFinishTime'])) ? $parsedJSON['oddFinishTime'] : '';
    	$evenOpenTime = (isset($parsedJSON['evenOpenTime'])) ? $parsedJSON['evenOpenTime'] : '';
    	$evenFinishTime = (isset($parsedJSON['evenFinishTime'])) ? $parsedJSON['evenFinishTime'] : '';
		$qualification = (isset($parsedJSON['qualification'])) ? $parsedJSON['qualification'] : '';

    	$isManCut = (isset($parsedJSON['isManCut'])) ? $parsedJSON['isManCut'] : '';
    	$isWomanCut = (isset($parsedJSON['isWomanCut'])) ? $parsedJSON['isWomanCut'] : '';
		$isEvening = (isset($parsedJSON['isEvening'])) ? $parsedJSON['isEvening'] : '';
    	$isColor = (isset($parsedJSON['isColor'])) ? $parsedJSON['isColor'] : '';
    	$isHighlight = (isset($parsedJSON['isHighlight'])) ? $parsedJSON['isHighlight'] : '';
		
		$manCutPrice = (isset($parsedJSON['manCutPrice'])) ? $parsedJSON['manCutPrice'] : '';
    	$womanCutPrice = (isset($parsedJSON['womanCutPrice'])) ? $parsedJSON['womanCutPrice'] : '';
		$colorizePrice = (isset($parsedJSON['colorizePrice'])) ? $parsedJSON['colorizePrice'] : '';
    	$hairdoPrice = (isset($parsedJSON['hairdoPrice'])) ? $parsedJSON['hairdoPrice'] : '';
		

		$shedule="";
		$shedule_names="`salon_id`";
		$shedule_values="";
    if($isOdd){
        $shedule.="По четным с $evenOpenTime до $evenFinishTime\n";
        $shedule_names.=", `isOdd`, `oddOpenTime`, `oddFinishTime`";
        $shedule_values.=", $isOdd, $oddOpenTime, $oddFinishTime";
    }
    if($isEven){
        $shedule.="По четным с $evenOpenTime до $evenFinishTime\n";
        $shedule_names.=", `isEven`, `evenOpenTime`, `evenFinishTime`";
        $shedule_values.=", $isEven, $evenOpenTime, $evenFinishTime";
    }
    if($isMonday){
        $shedule.=   "Понедельник с $monOpenTime до $monFinishTime\n";
        $shedule_names.=", `isMonday`, `monOpenTime`, `monFinishTime`";
        $shedule_values.=", $isMonday, $monOpenTime,  $monFinishTime";
    }
    if($isTuesday){
        $shedule.=  "Вторник     с $tueOpenTime до $tueFinishTime\n";
        $shedule_names.=", `isTuesday`, `tueOpenTime`, `tueFinishTime`";
        $shedule_values.=", $isTuesday, $tueOpenTime,  $tueFinishTime";
    }
    if($isWednesday){
        $shedule.="Среда       с $wedOpenTime до $wedFinishTime\n";
        $shedule_names.=", `isWednesday`, `wedOpenTime`, `wedFinishTime`";
        $shedule_values.=", $isWednesday, $wedOpenTime,  $wedFinishTime";
    }
    if($isThursday){
        $shedule.= "Четверг     с $thuOpenTime до $thuFinishTime\n";
        $shedule_names.=", `isThursday`, `thuOpenTime`, `thuFinishTime`";
        $shedule_values.=", $isThursday, $thuOpenTime,  $thuFinishTime";
    }
    if($isFriday){
        $shedule.=   "Пятница     с $friOpenTime до $friFinishTime\n";
        $shedule_names.=", `isFriday`, `friOpenTime`, `friFinishTime`";
        $shedule_values.=", $isFriday, $friOpenTime,  $friFinishTime";
    }
    if($isSaturday){
        $shedule.= "Суббота     с $satOpenTime до $satFinishTime\n";
        $shedule_names.=", `isSaturday`, `satOpenTime`, `satFinishTime`";
        $shedule_values.=", $isSaturday, $satOpenTime,  $satFinishTime";
    }
    if($isSunday){
        $shedule.=   "Воскресенье с $sunOpenTime до $sunFinishTime\n";
        $shedule_names.=", `isSunday`, `sunOpenTime`, `sunFinishTime`";
        $shedule_values.=", $isSunday, $sunOpenTime,  $sunFinishTime";
	}


	  	
		$newPin=makePassword("pin");
         $coord=getCoordinates($salonAddr);
        
        $result = mysql_query("INSERT INTO `salons`(`name`, 
													`address`, 
													`lat`, 
													`lng`, 
													`phone`, 
													`email`, 
													`logo_path`, 
													`shedule_text`,
													`registration_date`,
													`comments`) VALUES ('$newSalonName',
																				 '$newSalonAddress',
																				 '$coord[0]',
																				 '$coord[1]',
																				 '$newSalonPhone',
																				 '$newSalonEmail',
																				 '$uploadedSalonFilePath',
'$shedule',																				 
																				  NOW(),
																				 '$comments')")or die('Bad salon insert: ' . mysql_error());
		$qualif_id=1;
		//echo "12aq $qualification=".$qualification."|";

		if(stripos($qualification, "master")){
				$qualif_id=3;
		}else  if(stripos($qualification, "stylist")){
						$qualif_id=2;
					}else if(stripos($qualification,  "barber")){
									$qualif_id=1;
								}


		$salon_id=mysql_insert_id();

           // $result_qualif=  mysql_query("INSERT INTO `salon_specializations`(`barber_id`, `qualification_id`) VALUES ($barber_id, $qualif_id)") or die('Bad qualification insert: ' . mysql_error());
        if($isManCut=="true")
       		$result_qualif=  mysql_query("INSERT INTO `salon_specializations`(`salon_id`, `specialization_id`) VALUES ($salon_id, 1)") or die('Bad Sspecialization 1 insert: ' . mysql_error());
       	 if($isWomanCut=="true")
       		$result_qualif=  mysql_query("INSERT INTO `salon_specializations`(`salon_id`, `specialization_id`) VALUES ($salon_id, 2)") or die('Bad Sspecialization 2 insert: ' . mysql_error());
       	 if($isEvening=="true")
       		$result_qualif=  mysql_query("INSERT INTO `salon_specializations`(`salon_id`, `specialization_id`) VALUES ($salon_id, 3)") or die('Bad Sspecialization 3 insert: ' . mysql_error());
       	if($isColor=="true")
       		$result_qualif=  mysql_query("INSERT INTO `salon_specializations`(`salon_id`, `specialization_id`) VALUES ($salon_id, 4)") or die('Bad Sspecialization 4 insert: ' . mysql_error());
       	if($isHighlight=="true")
       		$result_qualif=  mysql_query("INSERT INTO `salon_specializations`(`salon_id`, `specialization_id`) VALUES ($salon_id, 5)") or die('Bad Sspecialization 5 insert: ' . mysql_error());
		
          $shedule_query = "INSERT INTO `salon_shedules`($shedule_names) VALUES (".$salon_id.$shedule_values.")";
          mysql_query($shedule_query) or die("bad salon_shedule insert ".mysql_error());
         // echo $shedule_query;

          mysql_query("INSERT INTO `salon_prices`(`salon_id`, `man_cut`, `woman_cut`, `colorize`, `hairdo`)
                     VALUES ($salon_id, '$manCutPrice','$womanCutPrice','$colorizePrice','$hairdoPrice')")or die ("bad salon price".mysql_error());
                     
		echo "pin:".$newPin;
    }
	
	if(strlen($dataType)>0&&$dataType=="showAll"){
		
		//echo json_encode(showBarbers("select * from barbers"));
		$barberz=array_merge(showBarbers("select * from barbers"),showSalons("select * from salons"));
		for($i=1;$i<=count($barberz);$i++)$indexez[]="$i";
		echo json_encode(array_combine($indexez,$barberz));
	}
}else{
	
	//$barberz=array_merge(showBarbers("select * from barbers"),showSalons("select * from salons"));
	//for($i=1;$i<=count($barberz);$i++)$indexez[]="$i";
	//echo json_encode(array_combine($indexez,$barberz));
	
	$specializationToSearch = "1|2";
		$arr=explode("|",$specializationToSearch);
		$str="(";
		for($z=0;$z<count($arr);$z++){
			if($z!=0)$str.=", ";
			$str.="'".$z."'";
		}
		$str.=")";
		$barberz=Array();
		$indexez=Array();
		
		$query="select distinct barber_id from barbers_specialization where specialization_id in $str ORDER BY barber_id";
		$result=mysql_query($query) or die(mysql_error());
		while($row=mysql_fetch_row($result)):
			$barberz=array_merge(showBarbers("select * from barbers where id=".$row[0]),$barberz);
		endwhile; 
		
		$query="select salon_id from salon_specializations where specialization_id in $str ORDER BY salon_id";
		$result=mysql_query($query) or die(mysql_error());
		while($row=mysql_fetch_row($result)):
			$barberz=array_merge(showSalons("select * from salons where id=".$row[0]),$barberz);
		endwhile; 
		
		for($i=1;$i<=count($barberz);$i++)$indexez[]="$i";
		
		echo json_encode(array_combine($indexez,$barberz));
	
}	
mysql_close();
?>